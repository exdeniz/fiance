require('selectric/public/jquery.selectric.js')

export function select () {
  $('.select').selectric({
    disableOnMobile: false
  })
}
