export function popup () {
  // Show credit popup

  $('.js-popup-credit').click(function () {
    $('.js-popup-credit-show').addClass('popupShow')
    $('.overlay').addClass('overlayShow')
  })

  $('.popupClose button').click(function () {
    $('.popup').removeClass('popupShow')
    $('.overlay').removeClass('overlayShow')
  })

  // Show refill popup

  $('.js-popup-refill').click(function () {
    $('.js-popup-refill-show').addClass('popupShow')
    $('.overlay').addClass('overlayShow')
  })

  $('.popupClose button').click(function () {
    $('.popup').removeClass('popupShow')
    $('.overlay').removeClass('overlayShow')
  })

  // Show popup Pay
  $('.js-showPopupPay').click(function () {
    $('.payPopup').addClass('payPopupShow')
    $('.overlay').addClass('overlayShow')
  })
  $('.payPopupClose button').click(function () {
    $('.payPopup').removeClass('payPopupShow')
    $('.overlay').removeClass('overlayShow')
  })

  // Show popup User
  $('.userItemAvatar').hover(function () {
    $(this).children('.userPopup').toggleClass('userPopupShow')
  })
}
