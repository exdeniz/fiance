export function dropSmile() {
    $('.chatSmileDropWrap').slimScroll({
        height: $('.chatSmileDrop').height(),
        railVisible: true,
        railColor: '#fff',
        railOpacity: 1,
        color: 'rgb(235, 235, 235)',
        size: '5px',
        alwaysVisible: true,
        borderRadius: '3px',
        railBorderRadius : '2px'
    })
    $('.chatMessageSendControlSmileButton').click(function(){
        $('.chatSmileDrop').toggleClass('chatSmileDropShow')
    })

}
