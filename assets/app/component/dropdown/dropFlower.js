export function dropFlower () {
  $('.chatFlowerDropWrap').slimScroll({
    height: $('.chatFlowerDrop').height(),
    railVisible: true,
    railColor: '#fff',
    railOpacity: 1,
    color: 'rgb(235, 235, 235)',
    size: '5px',
    alwaysVisible: true,
    borderRadius: '3px',
    railBorderRadius: '2px'
  })
  $('.chatMessageSendControlFlowerButton').click(function () {
    $('.chatFlowerDrop').toggleClass('chatFlowerDropShow')
  })
}
