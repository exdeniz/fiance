require('jquery')
require('jquery-slimscroll/jquery.slimscroll.js')

import { checkbox } from './component/checkbox/checkbox.js'
import { setting } from './component/setting/setting.js'
import { popup } from './component/popup/popup.js'
import { select } from './component/select/select.js'
import { dropFlower } from './component/dropdown/dropFlower.js'
import { dropSmile } from './component/dropdown/dropSmile.js'
import { mobile } from './component/mobile/mobile.js'

document.addEventListener('DOMContentLoaded', function () {
  $('.chatMessageWindowWrap').slimScroll({
    height: $('.chatMessageWindowWrap').height(),
    railVisible: true,
    railColor: '#fff',
    railOpacity: 1,
    color: 'rgb(235, 235, 235)',
    size: '5px',
    alwaysVisible: true,
    borderRadius: '3px',
    railBorderRadius: '2px'
  })

  dropFlower()
  dropSmile()
  checkbox()
  setting()
  popup()
  select()
  mobile()
})
